/*
	Copyright (C) 2018-2021 Paul Hocker. All rights reserved.
	Licensed under the MIT License. (See LICENSE.md in the project root for license information)
*/

import { dirname } from 'path';
import { Uri } from "vscode"; 
import * as fs from 'fs';


export default class PathUtils {

	/**
	 * Converts "file:///d%3A/blaba/a.x" to "d:\blaba\a.x"
	 */
	public static uriToFileSystemPath(uri:string):string {
		let newuri = Uri.parse(uri);
		return newuri.fsPath;
	}

	/**
	 * Converts "file:///d%3A/blaba/a.x" to "d:\blaba\a.x"
	 */
    public static uriToPlatformPath(uri: string): string {
        let newuri = Uri.parse(uri);
        return newuri.fsPath;
	}
	
	/**
     * Returns the Path from a Filename
     * @param filename 
     */
    public static GetPathFromFilename(filename: string) {
        return dirname(filename);
	}
	
	/**
	 * Returns True if the File Exists
	 * 
	 * Accomodates filenames that might have forward
	 * slashes in thier name for posix transformations
	 * 
	 */
	public static fileExists(filename: string) {

		// account for forward slashes on non-windows platforms 
        // if (process.platform != "win32") 
		// 	filename = filename.replace("\\", "");

		try {
			fs.accessSync(filename, fs.constants.F_OK);
			return true;
		} catch(err) {
			return false;
		}
	}

	/*
		Returns True if the Directory Exists
	*/
	public static directoryExists(name: string) {

		try {
			fs.accessSync(name, fs.constants.F_OK);
			return true;
		} catch(err) {
			return false;
		}
	}

	/*
		Create Directory if None Exists
	*/
	public static directoryCreate(name: string) {

        if (!this.directoryExists(name)) {
			fs.mkdirSync(name)
        }

	}

	/*
		Remove File if it Exists
	*/
	public static fileRemove(name: string) {

		if (this.fileExists(name)) {
			fs.unlinkSync(name);
		}

	}
}