# CHANGELOG

<!--- next entry here -->

## 0.7.6
2021-05-03

### Fixes

- **assembler:** add 5.20 support (edd395c71a2ddfc5c39bb571113b0e838d0cc88c)

### Other changes

- KickAssembler 5.20 support and some fixes (814e75a3ec6856e95f7176d5cb67d480c110ca33)
- Merge branch 'kickAss520Support' into 'master' (379114c4b19e1adf2ef98e9a1b985a7834550034)

## 0.7.5
2021-05-02

### Fixes

- **hover:** catch non existing assemblerinfo (77ccd3611efaf8330686f69094bcd6af362d0294)

### Other changes

- Merge branch 'fix/98/ignoreMissingPrerequesites' into 'master' (9b49d95db871382d903c818014884d643d9e6227)

## 0.7.4
2021-04-16

### Fixes

- **ci:** show changelog before publish (eaa51a67759fa8029868d6ce341565c3b49ae37c)

### Other changes

- fix(ci) : add changelog to build (4816c9974a5f2426e30acf1831e4f7e840a500b7)

## 0.7.3
2021-04-14

### Fixes

- **doc:** update release notes (0fa2141997216614d6f5c877089feb08e6d5a4b2)
- **snippet:** misleading snippet placeholders (daf7853b4951f4546c4fad184f4bc7bd1ac1f92c)

## 0.7.2
2021-04-11

### Fixes

- **references:** macro names were not found (e06849f49b0659d4b0748ed0303d33763ee8ddef)
- **definition:** recognize labels in imm mnemonic (3589e045453c01a65869fff3688efa8612ce94d9)
- **snippets:** errorif snippet misses a comma (3ac7c071bc9e7383ea87ec594411c5c5e1a7f730)
- **diagnostic:** support build errors in includes (d8819678daf0ef47c69681da2e47beb52f03674d)

## 0.7.1
2021-03-31

### Fixes

- **config:** better error checking for key settings (c70573f8d6f9cadd5e09f50997180e2b4894b320)

## 0.7.0
2021-03-27

### Features

- **assembler:** Add -binfile Support to Assembler (a18ac6f8edce2dc9cfb9ea4eee1414f4134fa44a)
- **diagnostic:** show build errors in diagnostic provider (4bf788f7a8c7a92117afbdd232d089bb62e4be90)

### Fixes

- added information for commit messages when contributing (6609a27219f9c6aef8fd1d0ae1707a3f6f50a34e)
- **fs:** some checks were still using old exists method (b58153ec9f916e208475b0c236a29c820e6d62a1)
- **ci:** add changelog to version stage (f8eb6ac1781995126ceb535435d1e55644445f36)
- **ci:** do not include other changes in final changelog (82de68d2b6ec33ce8206bf2d51187e13b7b83c7a)

## 0.6.4
2021-03-25

### Fixes

- **ci:** fix release link (ee1b7db5762aa7f67226c05cd8886d87527d733e)

## 0.6.3
2021-03-25

### Fixes

- **build:** changed version number (409eb86c92940370e28c97960ec1c087fb9ad2ef)

## 0.6.2
2021-03-25

### Fixes

- **folding:** support nested scope folding (37506e77c5719520d3c772ac1259c69fbfab37f5)
- **ci:** added ci build (2e61107fe4312ea16d342f82b6075ca7265ffcc7)
- **build:** added package-locks (c9795047c960c67d856dce86acb8e2f2dd633c07)