# Release Notes
## v0.7.x
### Name

For a full list of changes, please see the projects [Changelog](CHANGELOG.md) file.

We hope you are enjoying the Extension, and if you find any bugs, or would like to see a certain feature added, please feel free to use our [Trello Board](https://trello.com/b/vIsioueo/kick-assembler-vscode-ext#) or the [Gitlab](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/issues) issues page.

Enjoy!
